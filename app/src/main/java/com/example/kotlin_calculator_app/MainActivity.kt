package com.example.kotlin_calculator_app

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlin_calculator_app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var canAddOperation = false // индикатор для добавления знака операции
    private var canAddDecimal = true // индикатор для добавления точки к числам
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar!!.hide() // скрываем ActionBar у приложения
    }

    fun numberAction(view: View) // метод, который добавляет цифру в поле
    {
        if(view is Button) // если элемент view является кнопкой
        {
            if(view.text == ".") // если у полученного элемента есть точка
            {
                if(canAddDecimal) // если есть возможность добавить точку к числу
                    binding.operationsField.append(view.text) // добавляем число в поле operationsField

                canAddDecimal = false // запрещаем ставить точку
            }
            else
                binding.operationsField.append(view.text) // добавляем число в поле operationsField

            canAddOperation = true // разрещаем ставить знак операции
        }
    }

    fun operationAction(view: View) // метод, который добавляет знак операции в поле
    {
        if(view is Button && canAddOperation) // если элемент view является кнопкой и разрешено ставить знак операции
        {
            binding.operationsField.append(view.text) // добавляем знак в поле operationsField
            canAddOperation = false // запрещаем ставить знак операции
            canAddDecimal = true // разрешаем ставить точку к числу
        }
    }

    fun allClearAction(view: View) // метод, очищающий поле
    {
        binding.operationsField.text = "" // поле operationsField становится пустым
    }

    fun backSpaceAction(view: View) // метод, убирающий последний символ (число, знак и тд) в поле operationsField
    {
        val length = binding.operationsField.length() // узнаем длину строки в поле operationsField
        if(length > 0) // если длина строки больше 0,
            binding.operationsField.text = binding.operationsField.text.subSequence(0, length - 1) // убираем последний символ из строки
    }

    fun equalsAction(view: View) // метод, который срабатывает при нажатии на кнопку "="
    {
        binding.operationsField.text = calculateResults() // ставим в строку результат операции, описанный в методе calculateResults()
    }

    private fun calculateResults(): String // метод, который получает всех операций
    {
        val digitsOperators = digitsOperators() // переменная получает список чисел, с которыми мы будем работать, от метода digitsOperators()
        if(digitsOperators.isEmpty()) return "" // если список пуст, возвращаем пустую строку

        val timesDivision = timesDivisionCalculate(digitsOperators) // переменная получает список с результатами операций деления или умножения от метода timesDivisionCalculate()
        if(timesDivision.isEmpty()) return "" // если список пуст, возвращаем пустую строку

        val result = addSubtractCalculate(timesDivision) // переменная получает список с результатами операций сложения или вычитания от метода addSubtractCalculate()
        return result.toString() // возращаем список в виде строки
    }

    private fun addSubtractCalculate(passedList: MutableList<Any>): Float // метод, выполняющий сложения и вычитание,
    // с параметром изменяемого списка любого типа и возвращающий тип данных float
    {
        var result = passedList[0] as Float // переменная получает первый элемент списка в виде float

        for(i in passedList.indices) // для каждого элемента в списке
        {
            if(passedList[i] is Char && i != passedList.lastIndex) // если элемент списка равен символу и счетчик не равен последнему индексу списка
            {
                val operator = passedList[i] // переменная оператор получает знак операции
                val nextDigit = passedList[i + 1] as Float // переменная след. числа получает число в виде float от следующего индекса
                if (operator == '+') // если знак операции "+"
                    result += nextDigit // к результату прибавляется следующее число
                if (operator == '-') // если знак операции "-"
                    result -= nextDigit // от результата вычитается следующее число
            }
        }

        return result // возвращаем результат
    }

    private fun timesDivisionCalculate(passedList: MutableList<Any>): MutableList<Any> // метод, выполняющий умножение и деление,
    // с параметром изменяемого списка любого типа
    {
        var list = passedList // здесь находится список
        while (list.contains('×') || list.contains('÷')) // до тех пор, пока в списке есть знаки умножения или деления
        {
            list = calcTimesDiv(list) // переменная получает список с полученнными результатами после деления, умножения или от полученного числа и знака +/-
        // от метода calcTimesDiv()
        }
        return list // возвращаем список
    }

    private fun calcTimesDiv(passedList: MutableList<Any>): MutableList<Any>
    // метод, выполняющий умножение и деление, с параметром изменяемого списка любого типа
    {
        val newList = mutableListOf<Any>() // здесь находится список
        var restartIndex = passedList.size // индекс равен размеру получаемого списка

        for(i in passedList.indices) // для каждого элемента в списке
        {
            if(passedList[i] is Char && i != passedList.lastIndex && i < restartIndex)
            // если элемент списка равен символу и счетчик не равен последнему индексу списка и счетчик меньше, чем индекс, полученный от списка
            {
                val operator = passedList[i] // переменная оператор получает знак операции
                val prevDigit = passedList[i - 1] as Float // переменная предыдуш. числа получает число в виде float от предыдущего индекса
                val nextDigit = passedList[i + 1] as Float // переменная след. числа получает число в виде float от следующего индекса
                when(operator) // в зависимости, какой стоял знак операции между двумя числами
                {
                    '×' -> // знак умножения
                    {
                        newList.add(prevDigit * nextDigit) // в список попадает результат умножения двух чисел
                        restartIndex = i + 1 // Рестартиндекс увеличивается на 1
                    }
                    '÷' -> // знак деления
                    {
                        newList.add(prevDigit / nextDigit) // в список попадает результат деления двух чисел
                        restartIndex = i + 1 // Рестартиндекс увеличивается на 1
                    }
                    else -> // по дефолту
                    {
                        newList.add(prevDigit) // в список попадает предыдущее число
                        newList.add(operator) // в список попадает знак операции (+ или -)
                    }
                }
            }

            if(i > restartIndex) // если счетчик больше чем рестартиндекс
                newList.add(passedList[i]) // в список попадает элемент, полученный от списка в параметрах метода
        }

        return newList // возвращаем список
    }

    private fun digitsOperators(): MutableList<Any> // метод, работающий с числами, изменяемый список любого типа
    {
        val list = mutableListOf<Any>() // изменяемый список любого типа
        var currentDigit = "" // текущее число
        for(character in binding.operationsField.text) // для каждого символа, полученной строки operationsField
        {
            if(character.isDigit() || character == '.') // если символ является цифрой или десятичной точкой
                currentDigit += character // текущее число конкатенируется с символом
            else // в противном случае
            {
                list.add(currentDigit.toFloat()) // в список добавляется текущее число, переведенное в тип float
                currentDigit = "" // текущее число становится пустым
                list.add(character) // в список добавляется символ (то есть знак операции)
            }
        }

        if(currentDigit != "") // если текущее число не пустое
            list.add(currentDigit.toFloat()) // в список добавляется текущее число, переведенное в тип float

        return list // возвращаем список
    }
}